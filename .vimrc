set nocompatible

" タブの画面上での幅
set tabstop=4
" タブをスペースに展開
set expandtab
set shiftwidth=4
set backspace=indent,eol,start
set listchars=tab:.\ 
set list
set showcmd
set completeopt-=preview
set number

" 大文字小文字を区別して検索
set noic
" 検索結果のハイライト
set hlsearch

" インクリメンタルサーチ
set incsearch
set laststatus=2

" pastemodeへの切り替えトグル
set pastetoggle=<f5>

" カラースキーマ
syntax on
colorscheme elflord

"" キーマッピング----------------
nnoremap j gj
nnoremap k gk

" コマンドモード時のバインド
cmap <C-a> <Home>
cmap <C-e> <End>
cmap <C-f> <Right>
cmap <C-b> <Left>
cmap <C-d> <Delete>

" 挿入モードでのカーソル移動
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-h> <Left>
inoremap <C-l> <Right>

" 検索時にいつもvery magic モードを使う
nnoremap / /\v

" 連続ペースト用(ビジュアル選択しての上書きペーストを繰り返し行う)
vnoremap <silent> <C-p> "0p
" Ctrl+Lで単語単位で連続ペースト
nnoremap <silent> <C-l> ciw<C-r>0<ESC>

" QuickFixウィンドウやLocationウィンドウを閉じるキー (,ccでどちらも閉じる)
nnoremap <silent> ,cc :cclose<CR>:lclose<CR>

" ファイル補完に<C-@>
inoremap <C-@> <C-x><C-f>

"" オートコマンド系
autocmd FileType javascript,html setlocal sw=2 sts=2 ts=2 expandtab
autocmd FileType yaml setlocal sw=2 sts=2 ts=2 expandtab

autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags

# filetype対応インデントon
filetype plugin indent on
